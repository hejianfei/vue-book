module.exports = {
    devServer: {
      proxy: {
        '/api': {
          // 需要代理的url
          target: 'https://api.zhuishushenqi.com',
          changeOrigin: true,
          ws: true,
          pathRewrite: {
            '^/api': '/'
          }
        }
      }
    }
  }