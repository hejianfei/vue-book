import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/index'
  },
  {
    path: '/index',
    name: 'index',
    component: () => import("../views/index.vue")
  },
  {
    path: '/test',
    name: 'test',
    component: () => import("../views/test.vue")
  },
  {
    path: '/login',
    name: 'login',
    component: () => import("../views/login.vue"),
    meta:{
      footer:true
  }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import("../views/register.vue"),
    meta:{
      footer:true
  }
  },
  {
    path: '/category',
    name: 'category',
    component: () => import("../views/category.vue")
  },
  {
    path: '/detail',
    name: 'detail',
    component: () => import("../views/detail.vue")
  },
  {
    path: '/read',
    name: 'read',
    component: () => import("../views/read.vue"),
    meta:{
      footer:true
  }
  },
  {
    path: '/user',
    name: 'user',
    component: () => import("../views/user.vue"),
    // 独享路由守卫
    beforeEnter: (to, from, next) => {
      let login = window.sessionStorage.getItem("login");
      // 判断登录情况
      if(!login){
        next("/login");
        return 
      }else{
        next();
      }
    }
  },
  {
    path: '/search',
    name: 'search',
    component: () => import("../views/search.vue"),
    meta:{
      footer:true
  }
  },
  {
    path: '/rank',
    name: 'rank',
    component: () => import("../views/rank.vue")
  },
  {
    path: '/bookshelf',
    name: 'bookshelf',
    component: () => import("../views/bookshelf.vue")
  },
  {
    path: '/catlist',
    name: 'catlist',
    component: () => import("../views/catlist.vue")
  }
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
