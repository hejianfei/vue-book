import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import Axios from 'axios'
import RickText from 'vue-rich-text';
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false
import "./assets/css/reset.css"
import "./assets/font/font/iconfont.css"
Axios.defaults.baseURL = '/api'
Vue.use(Vant);
Vue.use(RickText);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
