import request from "./http";

export function getRanktype(params){
    return request({
        url:'/ranking/gender',
        method:"get",
        params
    })
}
// /:rankId
export function getRanking(id){
    return request({
        url:`/ranking/${id}`,
        method:"get",
        
    })
}
export function getBookCategories(params){
    return request({
        url:'/book/by-categories',
        method:"get",
        params
    })
}
export function getBook(id){
    return request({
        url:`/book/${id}`,
        method:"get",
        
    })
}
