import request from "./http";

export function getBookChapter(id){
    return request({
        url:`/mix-atoc/${id}`,
        method:"get",       
    })
}
export function getBookbtoc(id){
    return request({
        url:`/btoc${id}`,
        method:"get",
               
    })
}
export function getBookread(chapter){
    return request({
        url:`https://novel.kele8.cn/chapters2/56a8303e2aefdf397cbbf831/${chapter}`,
        method:"get",
               
    })
}
export function getchapers(){
    return request({
        url:`https://novel.kele8.cn/book-chapters/56a85badc27d9e8b7cacf3fe`,
        method:"get",
               
    })
}