import request from "./http";

export function getBookSearchhotword(params){
    return request({
        url:'/book/search-hotwords',
        method:"get",
        params
    })
}
export function getBookSearch(params){
    return request({
        url:'/book/fuzzy-search',
        method:"get",
        params
    })
}